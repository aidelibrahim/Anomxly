<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Banklist extends Model
{

  use Notifiable;

  protected $fillable = [
      'bank_name',
      'city',
      'ST',
      'CERT',
      'acquiring_institution',
      'closing_date',
      'updated_date'
  ];
}
