<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Charts;
use App\User;
use App\Banklist;
use App\DrugsOcc;
use App\Test;
use App\Ujian;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      // $chart = Charts::multi('line', 'chartjs')
      //     ->title("Ting goes SKKKRRRAAAAAA!")
      //     ->responsive(false)
      //     ->width(0)
      //     ->dimensions(0, 400)
      //     ->colors(['black', 'blue', 'red'])
      //     ->dataset('Element 1', [0,20,0])
      //     ->dataset('Element 2', [20,0,20])
      //     ->dataset('Element 3', [10,10,10])

          // ->labels(['One', 'Two', 'Three']);


          ////

      // $chart = Charts::database(User::all(), 'bar', 'highcharts')
      //     ->title("Ting goes SKKKRRRAAAAAA!")
      //     ->responsive(false)
      //     ->width(0)
      //     ->elementLabel('Total of Users')
      //     ->groupByDay('10', '2017', true);
      //
      //
      // $chart2 = Charts::database(User::all(), 'bar', 'highcharts')
      //     ->title("Games Played")
      //     ->responsive(false)
      //     ->width(0)
      //     ->elementLabel('Total of Users')
      //     ->groupBy('Game');
      //
      // $chart3 = Charts::realtime(route('data'), 1000, 'gauge', 'canvas-gauges')
      //     ->title("Real Time")
      //     ->responsive(false)
      //     ->width(0)
      //     ->elementLabel('Random');
      //
      // $chart4 = Charts::create('geo', 'google')
      //     ->title("Anomaly Detection")
      //     ->labels(['MY', 'FR', 'US', 'RU'])
      //     ->values([20, 45, 15, 30])
      //     ->elementLabel("Anomalies");
      //
      // $chart5 = Charts::database(Banklist::all(), 'bar', 'material')
      //     ->title("Bank List")
      //     ->responsive(false)
      //     ->width(0)
      //     ->elementLabel('Total of Users')
      //     ->groupBy('updated_date');
      //
      // $data = DrugsOcc::all();
      // $chart6 = Charts::create('bar', 'material')
      //     ->title("Drug Addicts in Construction Field by Year")
      //     ->width(0)
      //     ->elementLabel('Jumlah')
      //     ->labels($data->pluck('Tahun'))
      //     ->values($data->pluck('Binaan'))
      //     ->responsive(false);

      $ujian = Ujian::paginate(50);


      $chart7 = Charts::database(Ujian::all(), 'bar', 'fusioncharts')
          ->title("Log Result")
          ->responsive(false)
          ->width(0)
          ->labels(['Malicious', 'Normal'])
          ->groupBy('Label');

      $chart8 = Charts::database(Ujian::all(), 'pie', 'c3')
          ->title("Log Result")
          ->responsive(false)
          ->width(0)
          ->labels(['Malicious', 'Normal'])
          ->groupBy('Label');

      $chart9 = Charts::database(Ujian::all(), 'pie', 'c3')
          ->title("Attr9")
          ->responsive(false)
          ->width(0)
          ->labels(['Malicious', 'Normal'])
          ->groupBy('attr9');

      $chart10 = Charts::database(Ujian::all(), 'pie', 'c3')
          ->title("Policy Decision")
          ->responsive(false)
          ->width(0)
          ->labels(['Malicious', 'Normal'])
          ->groupBy('policy_decision');

      $chart11 = Charts::database(Ujian::all(), 'bar', 'fusioncharts')
          ->title("Action")
          ->responsive(false)
          ->width(0)
          ->labels(['Malicious', 'Normal'])
          ->groupBy('action');

        return view('home', [
            'chart7' => $chart7,
            'chart8' => $chart8,
            'chart9' => $chart9,
            'chart10' => $chart10,
            'chart11' => $chart11,
            'ujian' => $ujian]);

    }
}
