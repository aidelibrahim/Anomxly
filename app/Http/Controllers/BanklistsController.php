<?php

namespace App\Http\Controllers;

use App\Banklist;
use Illuminate\Http\Request;

class BanklistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banklist  $banklist
     * @return \Illuminate\Http\Response
     */
    public function show(Banklist $banklist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banklist  $banklist
     * @return \Illuminate\Http\Response
     */
    public function edit(Banklist $banklist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banklist  $banklist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banklist $banklist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banklist  $banklist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banklist $banklist)
    {
        //
    }
}
