<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class DrugsOcc extends Model
{
  use Notifiable;

  protected $fillable = [
      'Tahun',
      'BuruhAm',
      'Penganggur',
      'Pertanian',
      'Perkhidmatan',
      'Jualan',
      'Pengangkutan',
      'Teknikal',
      'Binaan',
      'Perkilangan',
      'Pengurusan',
      'Penuntut',
      'Perkeranian',
      'Hiburan',
      'RencamSambilan',
      'JUMLAH'
  ];
}
