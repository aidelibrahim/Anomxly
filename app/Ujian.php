<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Ujian extends Model
{
  use Notifiable;

  protected $fillable = [
      'action',
      'attr9',
      'decision_request_id',
      'from_resource_name',
      'from_resource_short_name',
      'host_name',
      'id',
      'policy_decision',
      'policy_id',
      'policy_name',
      'user_name',
      'label'
  ];
}
