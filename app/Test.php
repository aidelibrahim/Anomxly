<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
  use Notifiable;

  protected $fillable = [
      'data'
  ];


}
