<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if(!Schema::hasTable('banklists')){
        Schema::create('banklists', function (Blueprint $table) {
            $table->integer('CERT');
            $table->string('bank_name')->nullable();
            $table->string('city')->nullable();
            $table->string('ST')->nullable();
            $table->string('acquiring_institution')->nullable();
            $table->string('closing_date')->nullable();
            $table->string('updated_date')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banklists');
    }
}
