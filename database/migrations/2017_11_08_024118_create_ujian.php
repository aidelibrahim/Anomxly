<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUjian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ujian', function (Blueprint $table) {
          $table->string('action');
          $table->string('attr9');
          $table->string('decision_request_id');
          $table->string('from_resource_name');
          $table->string('from_resource_short_name');
          $table->string('host_name');
          $table->string('id');
          $table->string('policy_decision');
          $table->string('policy_id');
          $table->string('policy_name');
          $table->string('user_name');
          $table->string('label');

          $table->rememberToken();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ujian');
    }
}
