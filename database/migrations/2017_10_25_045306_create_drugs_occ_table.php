<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugsOccTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugs_occs', function (Blueprint $table) {
            $table->integer('Tahun');
            $table->string('BuruhAm')->nullable();
            $table->string('Penganggur')->nullable();
            $table->string('Pertanian')->nullable();
            $table->string('Perkhidmatan')->nullable();
            $table->string('Jualan')->nullable();
            $table->string('Pengangkutan')->nullable();
            $table->string('Teknikal')->nullable();
            $table->string('Binaan')->nullable();
            $table->string('Perkilangan')->nullable();
            $table->string('Pengurusan')->nullable();
            $table->string('Penuntut')->nullable();
            $table->string('Perkeranian')->nullable();
            $table->integer('Hiburan')->nullable();
            $table->string('RencamSambilan')->nullable();
            $table->string('JUMLAH')->nullable();


            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugs_occs');
    }
}
